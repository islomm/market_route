import React from "react";
import { Link, Outlet } from "react-router-dom";

const Layout = () => {
  return (
    <div className="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" class="navbar-brand" href="#">
          <h3>Home</h3>
        </Link>
        <ul class="navbar-nav">
          <li class="nav-item">
            <Link to="products" class="nav-link" href="#">
              <h3>Products</h3>
            </Link>
          </li>
          <li class="nav-item">
            <Link to="basket" class="nav-link" href="#">
              <h3> Basket</h3>
            </Link>
          </li>
        </ul>
      </nav>
      <Outlet />
    </div>
  );
};
export default Layout;
