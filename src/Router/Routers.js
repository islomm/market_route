import React from "react";
import Layout from "../component/Layout";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import Products from "../pages/Products";
import Basket from "../pages/Basket";
import ProductDetail from "../pages/ProductDetail";
const Routers = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="products" element={<Products />} />
          <Route path="/:id" element={<ProductDetail />} />
          <Route path="basket" element={<Basket />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default Routers;
