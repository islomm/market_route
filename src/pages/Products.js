import React, { memo } from "react";
import Axios from "../axios/Axios";
import { Link } from "react-router-dom";
const Products = () => {
  const { res, err, loader } = Axios();

  return (
    <>
      <div className="container">
        <h1 className="text-center pt-5">Products</h1>
      </div>
      <div className="row">
        {res?.map((item, index) => (
          <div key={index} className="mt-5 col-3">
            <div className="card shadow">
              <img
                height={300}
                className="card-img-top p-4"
                src={item.image}
              ></img>
              <div className="card-body">
                <h5 className="card-title">{item.title}</h5>
                <h3>
                  {item.price} <span className="text-success">$</span>{" "}
                </h3>
                <p className="card-text">{item.description.substring(1, 55)}</p>
                <Link to={`/${item.id}`}>
                  <p className="text-center">more ...</p>
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default memo(Products);
