import React, { memo } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Axios from "../axios/Axios";
import Products from "./Products";

const ProductDetail = () => {
  const { id } = useParams();
  const { res } = Axios();
  let product = null;
  res.map((item) => {
    if (item.id == id) {
      product = item;
    }
  });
  const back = useNavigate();
  return (
    <>
      <div className="d-flex pt-5 gap-5">
        <div className="border shadow p-4">
          <img width={400} height={400} src={product?.image} />
        </div>
        <div className="p-4 w-50 border shadow">
          <h1 className="text-center bg-danger text-white">
            {" "}
            ID :{product?.id}
          </h1>
          <h1>Category🛒 : {product?.category}</h1>
          <h2 className="pt-4">Title: {product?.title}</h2>
          <p>{product?.description}</p>
          <h3>
            Price : {product?.price} <span className="text-success">$</span>
          </h3>
          <h3>Rating : {product?.rating.rate}⭐</h3>
          <div className="d-flex justify-content-between">
            <h5 className="text-secondary">Left : {product?.rating.count} </h5>
            <button onClick={() => back(-1)} className="btn btn-danger">
              Back ↩️
            </button>
            <button className="btn btn-success">Add to Basket</button>
          </div>
        </div>
      </div>
    </>
  );
};

export default memo(ProductDetail);
